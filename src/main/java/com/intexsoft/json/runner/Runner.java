package com.intexsoft.json.runner;

import com.intexsoft.json.JsonParser;

import java.io.FileNotFoundException;

public class Runner {
    public static void main(String[] args) throws FileNotFoundException {
        System.out.println(JsonParser.parseFromFile("src/main/resources/example.json"));
    }
}
